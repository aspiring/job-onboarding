pipelineJob('accounts-summary-service') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://aspiring@bitbucket.org/aspiring/accounts-summary-service.git'
                    }
                    branch 'master'
                }
            }
        }
    }
}
pipelineJob('bank-accounts-service') {
    definition {
        cps {
            script(readFileFromWorkspace('bankAccountsJob.groovy'))
            sandbox()
        }
    }
}